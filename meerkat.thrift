typedef i64 Timestamp;


enum ErrorCode {
    NO_CONTENT = 0x10,
    PASTE_NOT_FOUND = 0x11,
    INVALID_ARGUMENTS = 0x12,
}


exception UserError {
    1: ErrorCode code,
    2: string message,
}


struct Paste {
    1: string id,
    2: string content,
    3: string source,
    4: Timestamp created_at,
    5: map<string, list<string>> meta,
}

service MeerkatService {
    string test();
    string commit_paste(1: string content) throws (1: UserError user_error);
    Paste get_paste(1: string paste_id) throws (1: UserError user_error);
    string render_paste(1: string paste_id);
    string preview_paste(1: string content);
    string proxy();
}
