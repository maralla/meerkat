import markdown
import logging
from takumi import Takumi, thrift_module, define_hook
from takumi_sqlalchemy import db
from takumi_http import pass_request
from takumi_thrift import Response

from .proxy import mod as proxy_mod
from .mappings import Paste
from .template import render_template

app = Takumi('MeerkatService')
UserError = thrift_module.UserError
ErrorCode = thrift_module.ErrorCode

db.init_app(app)
app.extend(proxy_mod)

CONCISE_LIMIT = 5

logger = logging.getLogger(__name__)


def _to_timestamp(dt):
    return int(dt.timestamp() * 1000)


@define_hook(event='before_api_call')
def argument_concise(ctx):
    concise_spec = ctx.conf.get('concise', [])
    ctx.__args_bak = ctx.args
    ctx.args = args = list(ctx.args)
    for spec in concise_spec:
        if 0 <= spec < len(args) and isinstance(args[spec], str) \
                and len(args[spec]) > CONCISE_LIMIT:
            args[spec] = ''.join([args[spec][:CONCISE_LIMIT], '...'])


@define_hook(event='api_called')
def reset_argument_concise(ctx):
    ctx.args = ctx.pop('__args_bak', [])


app.use(argument_concise)
app.use(reset_argument_concise)


def render_markdown(content):
    md = markdown.Markdown(
        extensions=['fenced_code', 'codehilite', 'admonition',
                    'tables', 'meta'],
        extension_configs={
            'codehilite': {
                'css_class': 'highlight',
                # 'noclasses': True
            }
        })
    html = md.convert(content)
    return html, md.Meta


def _get_paste(request, paste_id):
    paste_id = paste_id or request.qs.get('paste_id')
    if not paste_id:
        raise UserError(code=ErrorCode.INVALID_ARGUMENTS,
                        message='Missing paste_id.')
    paste = Paste.get(paste_id)
    if not paste:
        raise UserError(code=ErrorCode.PASTE_NOT_FOUND,
                        message='No such paste.')
    return paste


@app.api
def test():
    return 'ok'


@app.api_with_ctx(concise=[0])
@pass_request
def commit_paste(request, content):
    if not content:
        raise UserError(code=ErrorCode.NO_CONTENT, message='No content.')
    ua = request.headers.get('user-agent', 'unknown')
    return Paste.save(content, ua)


@app.api_with_ctx
@pass_request
def get_paste(request, paste_id):
    paste = _get_paste(request, paste_id)
    return thrift_module.Paste(
        id=paste.id,
        content=paste.content,
        source=paste.source,
        created_at=_to_timestamp(paste.created_at),
    )


@app.api(concise=[0])
def preview_paste(content):
    data, _ = render_markdown(content)
    return data


@app.api_with_ctx
@pass_request
def render_paste(request, paste_id):
    code = 200
    try:
        paste_id = paste_id or request.qs.get('paste_id')
        if not paste_id:
            code = 400
        else:
            paste = Paste.get(paste_id)
            if not paste:
                code = 404
            else:
                data, meta = render_markdown(paste.content)
                title = meta.get('title') or 'Meerkat'
                if isinstance(title, list):
                    title = title[0]
                html = render_template(
                    'paste/view.html', title=title, content=data)
    except Exception as e:
        code = 500
        logger.exception(e)
    if code != 200:
        html = render_template('paste/error.html', code=code)
    return Response(html, meta={
        'content-type': 'text/html', 'status_code': code})
