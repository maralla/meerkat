import os

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))

# For takumi-sqlalchemy.
DB_SETTINGS = {
    '': {
        'dsn': os.getenv(
            'MEERKAT_DB_DSN',
            'postgresql+psycopg2://meerkat:123@localhost/meerkat_dev')
    }
}

TEMPLATE_DIRECTORY = os.getenv(
    'MEERKAT_TEMPLATE', os.path.join(CURRENT_DIR, '..', 'assets', 'dist'))
