import cgi
import os

from takumi import TakumiModule, thrift_module
from takumi_thrift import Response
from takumi_http import pass_request
from urllib.parse import urlparse, urlunparse


mod = TakumiModule()
UserError = thrift_module.UserError
ErrorCode = thrift_module.ErrorCode


def _fill_path(path, url_prefix, url):
    if path.startswith('data:'):
        return path

    if path.startswith('//'):
        path = 'http:' + path
    elif path.startswith('/'):
        path = url_prefix + path
    elif not (path.startswith('http://') or path.startswith('https://')):
        path = os.path.dirname(url) + '/' + path
    return '/proxy?' + path


def _modify_link(links, attr, url_prefix, url):
    for link in links:
        href = link.attrs.get(attr)
        if not href:
            continue
        link.attrs[attr] = _fill_path(href, url_prefix, url)
        srcset = link.attrs.get('srcset')
        if srcset:
            srcset = ','.join([_fill_path(src.strip(), url_prefix, url)
                              for src in srcset.split(',')])
            link.attrs['srcset'] = srcset


def _adjust_url(doc, url_prefix, url):
    for attr, link in (
            ('a', 'href'),
            ('script', 'src'),
            ('link', 'href'),
            ('img', 'src'),
    ):
        _modify_link(doc.find_all(attr), link, url_prefix, url)
    return doc


def _domain_process(text, host, url_prefix):
    if host.endswith('google.com'):
        text = text.replace('/xjs/_/js',
                            '/proxy?{}/xjs/_/js'.format(url_prefix))
        text = text.replace("src='/images/",
                            "src='/proxy?{}/images/".format(url_prefix))
        text = text.replace("url=https://", "url=/proxy?https://")
        text = text.replace("url('https://", "url('/proxy?https://")
        text = text.replace('/gen_204', '/proxy?{}/gen_204'.format(url_prefix))
        text = text.replace('action="/search"', 'action="/googlesearch/proxy"')
    return text


@mod.api_with_ctx(timeout=25)
@pass_request
def proxy(request):
    headers = request.headers
    parts = urlparse(request.url)
    query = parts.query
    if parts.path == '/googlesearch/proxy':
        query = 'https://www.google.com/search?' + query
    if not query:
        raise UserError(code=ErrorCode.INVALID_ARGUMENTS,
                        message='Missing request url')
    parts = urlparse(query)
    import requests
    import bs4
    headers = dict(headers)
    headers['host'] = parts.netloc
    headers['authority'] = parts.netloc
    headers.pop('accept-encoding', None)
    url_prefix = urlunparse((parts.scheme, parts.netloc, '', '', '', ''))
    headers['referer'] = url_prefix
    res = requests.request(request.method, query, headers=headers, timeout=20)
    res.headers.pop('Transfer-Encoding', None)
    res.headers.pop('Content-Encoding', None)
    mime, extra = cgi.parse_header(
        res.headers.get('content-type', 'application/json'))
    headers = dict(res.headers)
    if mime == 'text/html':
        charset = extra.get('charset', 'utf-8')
        doc = bs4.BeautifulSoup(res.content.decode(charset), 'html.parser')
        content = str(_adjust_url(doc, url_prefix, query)).encode(charset)
    else:
        content = res.content
    text = ''.join(map(chr, content))
    text = _domain_process(text, parts.netloc, url_prefix)
    return Response(text, meta=headers)
