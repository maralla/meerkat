from jinja2 import FileSystemLoader, Environment
from takumi_config import config


class TemplateLoader(FileSystemLoader):
    def __init__(self):
        path = config.settings.get('TEMPLATE_DIRECTORY')
        super().__init__(path)


_template_env = Environment(loader=TemplateLoader())


def render_template(filename, **kwargs):
    t = _template_env.get_template(filename)
    return t.render(**kwargs)
