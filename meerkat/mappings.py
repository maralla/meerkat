import uuid
import sqlalchemy as sa
import datetime
import hashlib
from sqlalchemy.ext.declarative import declarative_base
from takumi_sqlalchemy import db


Base = declarative_base()


class Paste(Base):
    __tablename__ = 'meerkat_paste'

    id = sa.Column(sa.Text, primary_key=True)
    content = sa.Column(sa.Text)
    source = sa.Column(sa.Text, default='')
    digest = sa.Column(sa.Text, default='')
    created_at = sa.Column(sa.DateTime, default=datetime.datetime.now)

    @classmethod
    def save(cls, content, source=''):
        signature = '@'.join([content, source])
        digest = hashlib.sha256(signature.encode('utf-8')).hexdigest()
        with db.session_context() as session:
            paste = session.query(cls).filter(cls.digest == digest).first()
            if not paste:
                uid = uuid.uuid4().hex[:8]
                paste = cls(id=uid, content=content, source=source,
                            digest=digest)
                session.add(paste)
            else:
                uid = paste.id
            return uid

    @classmethod
    def get(cls, paste_id):
        with db.session_context() as session:
            return session.query(cls).get(paste_id)
