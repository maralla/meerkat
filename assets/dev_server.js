const proxy = require('http-proxy-middleware')
const Bundler = require('parcel-bundler')
const express = require('express')

const bundler = new Bundler('./templates/paste/index.html', { cache: false })
const app = express()

app.use(
  '/api',
  proxy({
    target: 'http://localhost:17800'
  })
)

app.use(bundler.middleware())

app.listen(Number(process.env.PORT || 1234))
