import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

const routes = [
  {path: '/paste/', component: App}
]

Vue.use(VueRouter)

export default new Vue({
  el: '#app',
  router: new VueRouter({
    mode: 'history',
    routes
  }),
  render: h => h('router-view')
})

